class AppImages {
  static const appLogo = "assets/images/app_icon.png";
  static const user = "assets/images/user.png";
  static const auth = "assets/images/auth.gif";
  static const forgotPasswordImage = "assets/images/forgot_pasword.png";
  static const loginImage = "assets/images/login_intro.png";
  static const registerImage = "assets/images/register_intro.jpg";
  static const otpImage = "assets/images/otp.png";
  static const splash = "assets/images/splash.gif";

  //
  static const onboarding1 = "assets/images/1.gif";
  static const onboarding2 = "assets/images/2.gif";
  static const onboarding3 = "assets/images/3.gif";
  static const welcome = "assets/images/welcome.gif";
  static const forget = "assets/images/forget.gif";

  //
  static const error = "assets/images/error.png";
  static const vendor = "assets/images/vendor.png";
  static const noProduct = "assets/images/no_product.png";
  static const product = "assets/images/product.png";
  static const emptyCart = "assets/images/no_cart.png";
  static const noReview = "assets/images/no_review.png";
  static const addressPin = "assets/images/delivery_address.png";
  static const deliveryParcel = "assets/images/delivery_parcel.png";
  static const deliveryVendor = "assets/images/delivery_vendor.png";

  //
  static const deliveryBoy = "assets/images/delivery_boy.png";
  static const pickupLocation = "assets/images/pickup_location.png";
  static const dropoffLocation = "assets/images/dropoff_location.png";
  static const driverCar = "assets/images/driver_car.png";
  static const refer = "assets/images/refer.png";


}
